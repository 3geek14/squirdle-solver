﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordleSolver
{
    static class EnumerableExtensions
    {
        public static IEnumerable<int> GetIndiciesOf<T>(this IEnumerable<T> source, T element)
        {
            int index = 0;
            foreach (var item in source)
            {
                if (item.Equals(element))
                {
                    yield return index;
                }
                ++index;
            }
        }
    }
}
