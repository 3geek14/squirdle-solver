﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordleSolver
{
    class Result
    {

        public Clue[] Clues { get; }
        public Result(Clue[] clues)
        {
            if (clues.Length != 5) throw new ArgumentException($"Found {clues.Length} clues, expected 5 clues.");
            this.Clues = clues;
        }

        public static Result Compare(string guess, string answer)
        {
            if (guess.Length != 5 || answer.Length != 5) throw new ArgumentException("Guesses and Answers should have 5 letters.");
            Clue[] clues = new Clue[5];
            for (int i = 0; i < 5; ++i)
            {
                if (guess[i] == answer[i]) clues[i] = Clue.Correct;
            }
            var lettersInGuess = guess
                .GroupBy(x => x)
                .Select(group => (letter: group.Key, count: group.Count()))
                .ToList();
            var lettersInAnswer = answer
                .GroupBy(x => x)
                .Select(group => (letter: group.Key, count: group.Count()))
                .ToList();
            foreach ((var letterG, var countG) in lettersInGuess)
            {
                bool match = false;
                var indicies = guess
                    .GetIndiciesOf(letterG)
                    .ToList();
                foreach ((var letterA, var countA) in lettersInAnswer)
                {
                    if (letterG == letterA)
                    {
                        match = true;
                        var min = Math.Min(countG, countA);
                        var greenCount = indicies
                            .Where(idx => clues[idx] == Clue.Correct)
                            .Count();
                        var others = indicies
                            .Where(idx => clues[idx] != Clue.Correct)
                            .ToList();
                        var yellows = others.Take(min - greenCount);
                        var blacks = others.Skip(min - greenCount);
                        foreach (var idx in yellows) clues[idx] = Clue.Position;
                        foreach (var idx in blacks) clues[idx] = Clue.Incorrect;
                    }
                }
                if (!match)
                {
                    foreach (var idx in indicies) clues[idx] = Clue.Incorrect;
                }
            }
            return new Result(clues);
        }

        public static Func<string, bool> MakeFilter(string guess, Result result)
        {
            return (string possibility) =>
            {
                for (int pos = 0; pos < 5; ++pos)
                {
                    var letter = guess[pos];

                    switch (result.Clues[pos])
                    {
                        case Clue.Correct:
                            if (possibility[pos] != letter) return false;
                            break;
                        case Clue.Position:
                            if (possibility[pos] == letter) return false;
                            var count = guess
                                .Zip(result.Clues, (let, clue) => (let, clue))
                                .Count(x => x.let == letter && x.clue != Clue.Incorrect);
                            if (possibility.Count(c => c == letter) < count) return false;
                            break;
                        case Clue.Incorrect:
                            if (possibility[pos] == letter) return false;
                            count = guess
                                .Zip(result.Clues, (let, clue) => (let, clue))
                                .Count(x => x.let == letter && x.clue != Clue.Incorrect);
                            if (possibility.Count(c => c == letter) != count) return false;
                            break;
                    }
                }
                return true;
            };
        }

        public override bool Equals(Object obj)
        {
            var other = obj as Result;
            return this.Clues.Zip(other.Clues).All(x => x.First == x.Second);
        }

        public override int GetHashCode()
        {
            return this.Clues.Aggregate(0, (hash, clue) => hash * 3 + (int)clue);
        }
    }

    enum Clue
    {
        Correct,
        Position,
        Incorrect,
    }
}
