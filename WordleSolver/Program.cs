﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace WordleSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            var answers = Properties.Resources.Answers.Split(',').ToList();
            var otherWords = Properties.Resources.OtherGuesses.Split(',');
            var dictionary = answers.Concat(otherWords).ToList();

            List<string> possibilities = answers;

            int guesses = 1;
            (string word, int worstCase, double entropy) bestGuess;

            while (true)
            {
                if (possibilities.Count == 1)
                {
                    Console.WriteLine("The only possible Pokemon is:");
                    Console.WriteLine(possibilities[0]);
                    guesses = 1;
                    possibilities = answers;
                    Console.ReadLine();
                    continue;
                }

                Console.WriteLine($"There are {possibilities.Count} potential Pokemon.");

                if (guesses == -1) // disabled
                {
                    Console.WriteLine("Start with Basculin");
                    Console.WriteLine();
                    bestGuess = ("stare", -1, double.NaN);
                }
                else
                {
                    bestGuess = BestGuess(dictionary, possibilities);

                    Console.WriteLine($"You can narrow this down to {bestGuess.worstCase} by guessing:");
                    Console.WriteLine(bestGuess.word);
                    Console.WriteLine();
                }

                Clue[] clues = new Clue[] { 0, 0, 0, 0, 0 };
                bool invalidInput = false;
                for (int i = 0; i < 5; ++i)
                {
                    do
                    {
                        Console.WriteLine($"Is letter {i + 1}:\n  [C] Correct\n  [P] wrong Position\n  [I] Incorrect");
                        char input = Console.ReadLine().ToUpper()[0];
                        switch (input)
                        {
                            case 'C':
                                clues[i] = Clue.Correct;
                                invalidInput = false;
                                break;
                            case 'P':
                                clues[i] = Clue.Position;
                                invalidInput = false;
                                break;
                            case 'I':
                                clues[i] = Clue.Incorrect;
                                invalidInput = false;
                                break;
                            default:
                                invalidInput = true;
                                break;
                        }
                    } while (invalidInput);
                }

                var clue = new Result(clues);
                var filter = Result.MakeFilter(bestGuess.word, clue);
                possibilities = possibilities.Where(filter).ToList();
                ++guesses;
            }
        }

        static void MainTimer(string[] args)
        {
            var answers = Properties.Resources.Answers.Split(',').ToList();
            var otherWords = Properties.Resources.OtherGuesses.Split(',');
            var dictionary = answers.Concat(otherWords).ToList();
            var rand = new Random();
            var times = new List<TimeSpan>();
            var sw = new Stopwatch();
            //int numCompares = dictionary.Count * answers.Count;
            int numCompares = answers.Count * answers.Count;

            //var guess = dictionary[rand.Next(0, dictionary.Count)];
            var guess = answers[rand.Next(0, answers.Count)];
            var answer = answers[rand.Next(0, answers.Count)];
            var clue = Result.Compare(guess, answer);

            for (int i = 0; i < numCompares; ++i)
            {
                sw.Restart();
                //guess = dictionary[rand.Next(0, dictionary.Count)];
                guess = answers[rand.Next(0, answers.Count)];
                answer = answers[rand.Next(0, answers.Count)];
                clue = Result.Compare(guess, answer);
                sw.Stop();
                times.Add(sw.Elapsed);
            }

            Console.WriteLine(times.Average(time => time.TotalMilliseconds));
            Console.WriteLine(times.Sum(time => time.TotalMilliseconds));
            Console.WriteLine(numCompares);
        }

        private static (string word, int worstCase, double entropy) BestGuess(List<string> dictionary, List<string> possibilities)
        {
            var rankedGuesses = dictionary
                .Select((guess, i) =>
                {
                    if (i % 100 == 0) Console.WriteLine(guess);
                    return (word: guess, classes: possibilities
                      .GroupBy(answer => Result.Compare(guess, answer))
                      .Select(equilClass => equilClass.ToList())
                      .ToList());
                })
                .Select(guess => (
                    word: guess.word,
                    worstCase: guess.classes.Max(equilClass => equilClass.Count),
                    entropy: Entropy(guess.classes)
                ))
                .OrderByDescending(guess => guess.entropy)
                .ToList();
            var goodGuesses = rankedGuesses
                .TakeWhile(guess => guess.entropy == rankedGuesses[0].entropy)
                .ToList();
            return goodGuesses
                .Where(guess => possibilities.Contains(guess.word))
                .Concat(goodGuesses)
                .First();
        }

        private static double Entropy(List<List<string>> classes)
        {
            var numPossibilities = classes.Sum(equilClass=>equilClass.Count());
            var entropy = -1 * classes
                .Select(equilClass => equilClass.Count / (double)numPossibilities)
                .Select(prob => prob * Math.Log(prob))
                .Sum();
            return entropy;
        }
    }
}
