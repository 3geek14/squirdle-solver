﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquirdleSolver
{
    class Result
    {

        public NumResult Generation { get; }
        public TypeResult Type1 { get; }
        public TypeResult Type2 { get; }
        public NumResult Height { get; }
        public NumResult Weight { get; }
        public Result(NumResult generation, TypeResult type1, TypeResult type2, NumResult height, NumResult weight)
        {
            Generation = generation;
            Type1 = type1;
            Type2 = type2;
            Height = height;
            Weight = weight;
        }

        public static List<Result> AllResults()
        {
            var all = new List<Result>();
            foreach (var generation in (NumResult[])Enum.GetValues(typeof(NumResult)))
            {
                foreach (var type1 in (TypeResult[])Enum.GetValues(typeof(TypeResult)))
                {
                    foreach (var type2 in (TypeResult[])Enum.GetValues(typeof(TypeResult)))
                    {
                        foreach (var height in (NumResult[])Enum.GetValues(typeof(NumResult)))
                        {
                            foreach (var weight in (NumResult[])Enum.GetValues(typeof(NumResult)))
                            {
                                all.Add(new Result(generation, type1, type2, height, weight));
                            }
                        }
                    }
                }
            }
            return all;
        }

        public static Func<Pokemon,bool> MakeFilter(Pokemon guess, Result result)
        {
            return (Pokemon mon) =>
            {
                switch (result.Generation)
                {
                    case NumResult.Correct:
                        if (mon.Generation != guess.Generation) return false;
                        break;
                    case NumResult.Up:
                        if (mon.Generation <= guess.Generation) return false;
                        break;
                    case NumResult.Down:
                        if (mon.Generation >= guess.Generation) return false;
                        break;
                }
                switch (result.Type1)
                {
                    case TypeResult.Correct:
                        if (mon.Type1 != guess.Type1) return false;
                        break;
                    case TypeResult.Incorrect:
                        if (mon.Type1 == guess.Type1 || mon.Type2 == guess.Type1) return false;
                        break;
                    case TypeResult.Swap:
                        if (mon.Type2 != guess.Type1) return false;
                        break;
                }
                switch (result.Type2)
                {
                    case TypeResult.Correct:
                        if (mon.Type2 != guess.Type2) return false;
                        break;
                    case TypeResult.Incorrect:
                        if (mon.Type1 == guess.Type2 || mon.Type2 == guess.Type2) return false;
                        break;
                    case TypeResult.Swap:
                        if (mon.Type1 != guess.Type2) return false;
                        break;
                }
                switch (result.Height)
                {
                    case NumResult.Correct:
                        if (mon.Height != guess.Height) return false;
                        break;
                    case NumResult.Up:
                        if (mon.Height <= guess.Height) return false;
                        break;
                    case NumResult.Down:
                        if (mon.Height >= guess.Height) return false;
                        break;
                }
                switch (result.Weight)
                {
                    case NumResult.Correct:
                        if (mon.Weight != guess.Weight) return false;
                        break;
                    case NumResult.Up:
                        if (mon.Weight <= guess.Weight) return false;
                        break;
                    case NumResult.Down:
                        if (mon.Weight >= guess.Weight) return false;
                        break;
                }
                return true;
            };
        }
    }

    enum NumResult
    {
        Correct,
        Up,
        Down,
    }
    enum TypeResult
    {
        Correct,
        Incorrect,
        Swap,
    }
}
