﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquirdleSolver
{
    class Pokemon
    {
        public string Name { get; }
        public int Generation { get; }
        public Type Type1 { get; }
        public Type Type2 { get; }
        public decimal Height { get; }
        public decimal Weight { get; }

        public Pokemon(string name, int generation, Type type1, Type type2, decimal height, decimal weight)
        {
            this.Name = name;
            this.Generation = generation;
            this.Type1 = type1;
            this.Type2 = type2;
            this.Height = height;
            this.Weight = weight;
        }
        public Pokemon(string name, int generation, Type type1, decimal height, decimal weight)
        {
            this.Name = name;
            this.Generation = generation;
            this.Type1 = type1;
            this.Type2 = Type.NULL;
            this.Height = height;
            this.Weight = weight;
        }

        public static Type Parse(string type)
        {
            if (type.Equals("")) return Type.NULL;
            if (type.Equals("Normal")) return Type.Normal;
            if (type.Equals("Fire")) return Type.Fire;
            if (type.Equals("Water")) return Type.Water;
            if (type.Equals("Electric")) return Type.Electric;
            if (type.Equals("Grass")) return Type.Grass;
            if (type.Equals("Ice")) return Type.Ice;
            if (type.Equals("Fighting")) return Type.Fighting;
            if (type.Equals("Poison")) return Type.Poison;
            if (type.Equals("Ground")) return Type.Ground;
            if (type.Equals("Flying")) return Type.Flying;
            if (type.Equals("Psychic")) return Type.Psychic;
            if (type.Equals("Bug")) return Type.Bug;
            if (type.Equals("Rock")) return Type.Rock;
            if (type.Equals("Ghost")) return Type.Ghost;
            if (type.Equals("Dragon")) return Type.Dragon;
            if (type.Equals("Dark")) return Type.Dark;
            if (type.Equals("Steel")) return Type.Steel;
            if (type.Equals("Fairy")) return Type.Fairy;
            throw new ArgumentException("Type not found.");
        }
    }

    enum Type
    {
        NULL,
        Normal,
        Fire,
        Water,
        Electric,
        Grass,
        Ice,
        Fighting,
        Poison,
        Ground,
        Flying,
        Psychic,
        Bug,
        Rock,
        Ghost,
        Dragon,
        Dark,
        Steel,
        Fairy,
    }
}
