﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SquirdleSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            var mons = new List<Pokemon>();
            var dex = Properties.Resources.pokedex
                .Split('\n');
            for (int i = 1; i < dex.Length; ++i)
            {
                var mon = dex[i].Split(',');
                mons.Add(new Pokemon(
                    mon[0],
                    int.Parse(mon[1]),
                    Pokemon.Parse(mon[2]),
                    Pokemon.Parse(mon[3]),
                    decimal.Parse(mon[4]),
                    decimal.Parse(mon[5])));
            }
            var results = Result.AllResults();
            var rand = new Random();

            //Pokemon guess;
            //Result clue;
            //Func<Pokemon, bool> filter;
            List<Pokemon> possibilities = mons;//.Where(mon => mon.Generation == 1).ToList();

            int counter = 1;
            (Pokemon mon, int count) bestGuess;

            while (true)
            {
                if (possibilities.Count == 1)
                {
                    Console.WriteLine("The only possible Pokemon is:");
                    Console.WriteLine(possibilities[0].Name);
                    counter = 1;
                    possibilities = mons;
                    Console.ReadLine();
                    continue;
                }

                Console.WriteLine($"There are {possibilities.Count} potential Pokemon.");

                if (counter == 1)
                {
                    Console.WriteLine("Start with Basculin");
                    Console.WriteLine();
                    bestGuess = (mons.First(mon => mon.Name.Equals("Basculin")), -1);
                }
                else
                {
                    var rankedGuesses = mons
                        .Select(guess => (mon: guess, count: results
                            .Select(result =>
                            {
                                var filter = Result.MakeFilter(guess, result);
                                return possibilities.Count(filter);
                            })
                            .Max()))
                        .OrderBy(guess => guess.count)
                        .ToList();
                    var goodGuesses = rankedGuesses
                        .TakeWhile(guess => guess.count == rankedGuesses[0].count)
                        .OrderBy(guess => rand.Next())
                        .ToList();
                    bestGuess = goodGuesses
                        .Where(guess => possibilities.Contains(guess.mon))
                        .Concat(goodGuesses)
                        .First();

                    Console.WriteLine($"You can narrow this down to {bestGuess.count} by guessing:");
                    Console.WriteLine(bestGuess.mon.Name);
                    Console.WriteLine();
                }

                NumResult gen = 0, height = 0, weight = 0;
                TypeResult type1 = 0, type2 = 0;
                bool invalidInput = false;
                do
                {
                    Console.WriteLine("Is the generation:\n  [C] Correct\n  [U] Higher than your guess\n  [D] Lower than your guess");
                    char input = Console.ReadLine().ToUpper()[0];
                    switch (input)
                    {
                        case 'C':
                            gen = NumResult.Correct;
                            invalidInput = false;
                            break;
                        case 'U':
                            gen = NumResult.Up;
                            invalidInput = false;
                            break;
                        case 'D':
                            gen = NumResult.Down;
                            invalidInput = false;
                            break;
                        default:
                            invalidInput = true;
                            break;
                    }
                } while (invalidInput);
                do
                {
                    Console.WriteLine("Is the first type:\n  [C] Correct\n  [I] Incorrect\n  [S] In the wrong place");
                    char input = Console.ReadLine().ToUpper()[0];
                    switch (input)
                    {
                        case 'C':
                            type1 = TypeResult.Correct;
                            invalidInput = false;
                            break;
                        case 'I':
                            type1 = TypeResult.Incorrect;
                            invalidInput = false;
                            break;
                        case 'S':
                            type1 = TypeResult.Swap;
                            invalidInput = false;
                            break;
                        default:
                            invalidInput = true;
                            break;
                    }
                } while (invalidInput);
                do
                {
                    Console.WriteLine("Is the second type:\n  [C] Correct\n  [I] Incorrect\n  [S] In the wrong place");
                    char input = Console.ReadLine().ToUpper()[0];
                    switch (input)
                    {
                        case 'C':
                            type2 = TypeResult.Correct;
                            invalidInput = false;
                            break;
                        case 'I':
                            type2 = TypeResult.Incorrect;
                            invalidInput = false;
                            break;
                        case 'S':
                            type2 = TypeResult.Swap;
                            invalidInput = false;
                            break;
                        default:
                            invalidInput = true;
                            break;
                    }
                } while (invalidInput);
                do
                {
                    Console.WriteLine("Is the height:\n  [C] Correct\n  [U] Higher than your guess\n  [D] Lower than your guess");
                    char input = Console.ReadLine().ToUpper()[0];
                    switch (input)
                    {
                        case 'C':
                            height = NumResult.Correct;
                            invalidInput = false;
                            break;
                        case 'U':
                            height = NumResult.Up;
                            invalidInput = false;
                            break;
                        case 'D':
                            height = NumResult.Down;
                            invalidInput = false;
                            break;
                        default:
                            invalidInput = true;
                            break;
                    }
                } while (invalidInput);
                do
                {
                    Console.WriteLine("Is the weight:\n  [C] Correct\n  [U] Higher than your guess\n  [D] Lower than your guess");
                    char input = Console.ReadLine().ToUpper()[0];
                    switch (input)
                    {
                        case 'C':
                            weight = NumResult.Correct;
                            invalidInput = false;
                            break;
                        case 'U':
                            weight = NumResult.Up;
                            invalidInput = false;
                            break;
                        case 'D':
                            weight = NumResult.Down;
                            invalidInput = false;
                            break;
                        default:
                            invalidInput = true;
                            break;
                    }
                } while (invalidInput);
                var clue = new Result(gen, type1, type2, height, weight);
                var filter = Result.MakeFilter(bestGuess.mon, clue);
                possibilities = possibilities.Where(filter).ToList();
                ++counter;
            }
        }
    }
}
